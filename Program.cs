﻿using System;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            string month = "";
            Console.WriteLine("In what month were you born?");
            month = Console.ReadLine();
            Console.WriteLine($"Ahh, so you were born in {month}");


            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
